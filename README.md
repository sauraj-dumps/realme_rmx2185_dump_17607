## full_oppo6765-user 10 QP1A.190711.020 bedd37e98646d3a1 release-keys
- Manufacturer: realme
- Platform: mt6765
- Codename: RMX2185
- Brand: realme
- Flavor: full_oppo6765-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: 0510_202006261907
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 
- OTA version: RMX2185_11.A.51_0510_202006261907
- Branch: RMX2185_11.A.51_0510_202006261907
- Repo: realme_rmx2185_dump_17607


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
